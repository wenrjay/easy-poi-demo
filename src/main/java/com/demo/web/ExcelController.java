package com.demo.web;

import com.demo.model.HouseBuildingInfoExcelVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

@RestController
@Slf4j
public class ExcelController {

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public void down() throws IOException {
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        response.setHeader("content-Type", "application/vnd.ms-excel");
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("楼盘数据-模板", "楼盘模板"),
                HouseBuildingInfoExcelVO.class, new ArrayList<>());
        OutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        outputStream.close();
    }

}
