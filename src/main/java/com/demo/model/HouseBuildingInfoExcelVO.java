package com.demo.model;

import lombok.*;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.math.BigDecimal;
import java.util.Date;

@Data
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HouseBuildingInfoExcelVO {

    /**
     * 楼盘id
     */
    private Long id;
    /**
     * 楼盘code
     */
    @Excel(name = "楼盘序号", height = 20, width = 20)
    private String code;

    /**
     * 楼盘名称
     */
    @Excel(name = "楼盘名称", height = 20, width = 20)
    private String buildingTitle;
    /**
     * 楼盘地址
     */
    @Excel(name = "楼盘地址" , height = 20, width = 20)
    private String buildingAddress;
    /**
     * 楼盘所在省
     */
    @Excel(name = "楼盘所在省份" , height = 20, width = 20)
    private String buildingProvince;

    /**
     * 楼盘所在市
     */
    @Excel(name = "楼盘所在市",  height = 20, width = 20)
    private String buildingCity;

    /**
     * 楼盘所区县
     */
    @Excel(name = "楼盘所在区", height = 20, width = 20)
    private String buildingDistrict;

    /**
     * 楼盘所在街道
     */
    @Excel(name = "楼盘所在街道", height = 20, width = 20)
    private String buildingStreet;

    /**
     * 楼盘参考单价(元/㎡)
     */
    @Excel(name = "楼盘参考单价(元/㎡)", height = 20, width = 20)
    private BigDecimal buildingSuggestPrice;

    /**
     * 楼盘开发商
     */
    @Excel(name = "楼盘开发商", height = 20, width = 20)
    private String buildingDevelopers;

    /**
     * 楼盘开盘时间
     */
    @Excel(name = "楼盘开发商", height = 20, width = 20)
    private Date buildingOepnTime;

    /**
     * 楼盘交房时间
     */
    @Excel(name = "楼盘交房时间", height = 20, width = 20)
    private Date buildingDeliveryTime;

    /**
     * 楼盘装修类型
     */
    @Excel(name = "楼盘装修类型", height = 20, width = 20)
    private String buildingDecorateType;

    /**
     * 楼盘产权年限
     */
    @Excel(name = "楼盘产权年限", height = 20, width = 20)
    private Integer buildingYears;

    /**
     * 楼盘容积率
     */
    @Excel(name = "楼盘容积率", height = 20, width = 20)
    private BigDecimal buildingPlotRatio;

    /**
     * 楼盘绿化率
     */
    @Excel(name = "楼盘绿化率", height = 20, width = 20)
    private BigDecimal buildingGreenRatio;

    /**
     * 楼盘车位比
     */
    @Excel(name = "楼盘车位比", height = 20, width = 20)
    private BigDecimal buildingParkingRatio;

    /**
     * 物业公司
     */
    @Excel(name = "物业公司", height = 20, width = 20)
    private String propertyCompany;

    /**
     * 物业费
     */
    @Excel(name = "物业费(元/㎡)", height = 20, width = 20)
    private BigDecimal propertyCost;

    /**
     * 楼盘标签
     */
    @Excel(name = "楼盘标签", height = 20, width = 20)
    private String buildingTag;
    /**
     * 物业类型
     */
    @Excel(name = "物业类型", height = 20, width = 20)
    private String property;

    /**
     * 水电燃气
     */
    @Excel(name = "水电燃气", height = 20, width = 20)
    private String waterManagement;
    /**
     * 供暧
     */
    @Excel(name = "供暧", height = 20, width = 20)
    private String heatingManagement;
    /**
     * 预算许可证
     */
    @Excel(name = "预算许可证", height = 20, width = 20)
    private String houseLicence;
    /**
     * 发证日期
     */
    @Excel(name = "发证日期", height = 20, width = 20)
    private Long houseLicenceTime;
    /**
     * 证件楼栋
     */
    @Excel(name = "证件楼栋", height = 20, width = 20)
    private Integer houseLicenceStoried;

    /**
     * 热销户型
     */
    @Excel(name = "热销户型", height = 20, width = 20)
    private String hotSellingHouse;


}
